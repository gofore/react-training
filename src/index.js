import React from 'react';
import ReactDOM from 'react-dom';

ReactDOM.render(
  <h1>Welcome to React training!</h1>,
  document.getElementById("root")
);
